#  Guide to edo9300s Make Forwarder DSi

This is a guide that will explain you how to create DSi ROM forwarders which allow you to add NDS ROM files to your DSi System Menu via HiyaCFW.

See [here](https://nmoleo.gitlab.io/guide-to-edo9300s-make-forwarder-dsi/)
