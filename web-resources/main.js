function scrollDown() {
	document.getElementById("body").scrollIntoView({block: 'end', behavior: 'smooth'});
}

function scrollUp() {
	document.getElementById("body").scrollIntoView({block: 'start', behavior: 'smooth'});
}

function scrollBelowHeader() {
	document.getElementById("main").scrollIntoView({block: 'start', behavior: 'smooth'});
}

var button;

function init() {
	button = document.getElementById('top-btn');
	button.addEventListener('click', scrollUp);

	fadeIn(document.getElementById('h1'), 30);
	fadeIn(document.getElementById('h2'), 30);
	fadeIn(document.getElementById('p'), 30);
	fadeIn(document.getElementById('cfw-btn'), 30);
	fadeIn(document.getElementById('menu'), 30);
	fadeIn(document.getElementById('downArrow'), 100);
}

function fadeIn(element, time) {
	var op = 0.0; 
	var timer = setInterval(function () {
		if (op >= 1){
			clearInterval(timer);
		}
		element.style.opacity = op;
		op += 0.01;
	}, time);
}

function fadeOut(element, time) {
	var op = 1.0;
	var timer = setInterval(function () {
		if (op == 0.0){
			clearInterval(timer);
		}
		element.style.opacity = op;
		op -= 0.01;
	}, time);
}

var onScroll = function() {
	var y = window.scrollY;
	
	if (y >= 200) {
		button.style.opacity = 1.0;
	}
	else {
		button.style.opacity = 0.0;
	}
}

window.addEventListener('scroll', onScroll);